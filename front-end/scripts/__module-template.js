// Module representing a store of data.
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function(window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    function RenameThisModuleTemplateConstructorFunction() {
        console.log('running the RenameThisModuleTemplateConstructorFunction function');
    }

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.RenameThisModuleTemplateConstructorFunction = RenameThisModuleTemplateConstructorFunction;
    window.App = App;
})(window);