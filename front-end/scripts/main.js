// Main application JS module.
//
// Bootstraps other class modules.
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function(window) {
    'use strict';

    // fetch the namespace object
    //
    var App = window.App;

    // fetch the class constructor objects
    // to make new'ing a little easier
    //
    var Hive = App.Hive;
    var HiveStore = App.HiveStore;
    var HiveEntriesHandler = App.HiveEntriesHandler;
    var HiveListHandler = App.HiveListHandler;
    var UIElementSelectors = App.UIElementSelectors;

    function createHiveWithEntries(...entries) {
        var hive = new Hive();
        entries.forEach (function(e) {
            hive.addEntry(e);
        });

        return hive;
    }

    // spin up a hive store and give it
    // some test data
    //
    var hiveStore = new HiveStore();
    hiveStore.add('Hive 1', new Hive(
        'Entry 1'
    ));
    hiveStore.add('Hive 2', new Hive(
        'Entry 1',
        'Entry 2'
    ));
    hiveStore.add('Hive 3', new Hive(
        'Entry 1',
        'Entry 2',
        'Entry 3'
    ));
    hiveStore.add('Hive 4', new Hive(
        'Entry 1',
        'Entry 2',
        'Entry 3',
        'Entry 4'
    ));
    hiveStore.add('Hive 5', new Hive(
        'Entry 1',
        'Entry 2',
        'Entry 3',
        'Entry 4',
        'Entry 5'
    ));

    // make handler objects
    //
    var selectors = new UIElementSelectors();
    var hiveEntriesHandler = new HiveEntriesHandler(
        selectors.HiveEntriesPanelSelector,
        selectors.HiveEntriesSelector);
    var hiveListHandler = new HiveListHandler(selectors.HiveListSelector, hiveStore);

    // provide hive list to user for selection
    hiveListHandler.loadHiveList();

    // provide diary entries to user after hive is selected
    hiveListHandler.addChangeHandler(hiveEntriesHandler.loadEntryList.bind(hiveEntriesHandler));
})(window);