// Module representing a hive.
//
// A hive has
// - A list of diary entries
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function(window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    function Hive(...entries) {
        console.log('running the Hive function');

        this._entries = {};
        for (var e of entries) {
            this._entries[e] = e;
        }
    }

    // instance method to add a diary entry to the hive
    // - entryKey: uniquely identifies a diary entry (e.g., datetime)
    // - entry: diary entry to store
    Hive.prototype.addEntry = function(entryKey, entry) {
        this._entries[entryKey] = entry;
    }

    // instance method to retrieve all diary entries for the hive
    // - returns: Object
    Hive.prototype.getAllEntries = function() {
        return this._entries;
    }

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.Hive = Hive;
    window.App = App;
})(window);