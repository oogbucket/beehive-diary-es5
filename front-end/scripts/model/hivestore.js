// Module representing a collection of hives.
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function(window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    function HiveStore() {
        console.log('running the HiveStore function');

        // make a place to put the data this object stores,
        // being a DataStore
        //
        this.data = {};
    }

    // instance method to add a hive to the store
    // - hiveKey: uniquely identifies a hive
    // - hive: Hive object to store
    HiveStore.prototype.add = function(hiveKey, hive) {
        this.data[hiveKey] = hive;
    }

    // instance method to remove a hive from the store
    // - hiveKey: uniquely identifies a hive
    //
    HiveStore.prototype.remove = function(hiveKey) {
        delete this.data[hiveKey];
    }

    // instance method to retrieve a specific hive from the store
    // - hiveKey: uniquely identifies a hive
    // - returns: Hive identified by 'hiveKey'
    HiveStore.prototype.get = function(hiveKey) {
        return this.data[hiveKey];
    }

    // instance method to retrieve all hives from the store
    // - returns: Object
    HiveStore.prototype.getAll = function(hiveKey) {
        return this.data;
    }

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.HiveStore = HiveStore;
    window.App = App;
})(window);