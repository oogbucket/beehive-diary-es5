// Module for managing the data entry form.
// - Loads the hive selection dropdown list
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function(window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    // Parameters:
    // - selector: An XPath string referring to a DOM element on the form.
    function FormHandler(selector) {
        if (!selector) {
            throw new Error('No selector provided');
        }

        this.$formSelector = $(selector);
    }

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.FormHandler = FormHandler;
    window.App = App;
})(window);