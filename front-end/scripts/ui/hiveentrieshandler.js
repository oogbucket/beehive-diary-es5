// Module for managing the list of diary entries for a hive on the data entry form.
// - Loads the hive diary entry list
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function (window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    // Parameters:
    // - selector: An XPath string referring to a DOM element on the form.
    function HiveEntriesHandler(panelSelector, listSelector) {
        if (!panelSelector) {
            throw new Error('No panel selector provided');
        } else {
            this.$panelSelector = $(panelSelector);
            if (this.$panelSelector.length === 0) {
                throw new Error('Could not find element with selector: ' + panelSelector);
            }
        }

        if (!listSelector) {
            throw new Error('No list selector provided');
        } else {
            this.$listSelector = $(listSelector);
            if (this.$listSelector.length === 0) {
                throw new Error('Could not find element with selector: ' + listSelector);
            }
        }
    }

    // instance method to inject an HTML <ul> element with diary entries
    // - hive: an object isomorphic to Hive
    HiveEntriesHandler.prototype.loadEntryList = function (hive) {

        // get all the diary entries
        var entries = hive.getAllEntries();

        // turn the diary entries into HTML
        var options = [];
        for (var e in entries) {
            options.push(`<li class="list-group-item">${e}</li>`);
        }

        // load the new options list
        this.$listSelector.html(options.join(''));

        // show the panel only if there are some entries
        if (!options || options.length == 0)
            this.$panelSelector.addClass("diary-no-entries");
        else
            this.$panelSelector.removeClass("diary-no-entries");
    };

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.HiveEntriesHandler = HiveEntriesHandler;
    window.App = App;
})(window);