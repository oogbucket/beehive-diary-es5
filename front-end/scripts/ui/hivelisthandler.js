// Module for managing the list of hives on the data entry form.
// - Loads the hive selection dropdown list
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function (window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    // Parameters:
    // - selector: An XPath string referring to a DOM element on the form.
    function HiveListHandler(selector, hiveStore) {
        if (!selector) {
            throw new Error('No selector provided');
        }

        this.$listSelector = $(selector);
        this._hiveStore = hiveStore;
    }

    // instance method to handle selection of an item from the list
    HiveListHandler.prototype.addChangeHandler = function(fn) {
        // closure-ize interesting info
        var my$hiveStore = this._hiveStore;
        var my$listSelector = this.$listSelector;

        // when the selection changes on the dropdown list,
        // find the hive associated with the selected name
        // and invoke the specified function ('fn') on it
        my$listSelector.on('change', function(event) {
            var hiveName = my$listSelector.val();
            var hive = my$hiveStore.get(hiveName);
            fn(hive);
        });
    }

    // instance method to load an HTML <select> element with hive names
    // - hiveStore: an object isomorphic to HiveStore
    HiveListHandler.prototype.loadHiveList = function () {
        var my$hiveStore = this._hiveStore;

        // get all the hive names
        var hiveNames = Object.keys(my$hiveStore.getAll());

        // turn the hive names into HTML
        var options = [];
        for (var i = 0; i < hiveNames.length; i++) {
            options.push(`<option>${hiveNames[i]}</option>`);
        }

        // load the new options list and unselect
        // the first entry
        this.$listSelector.html(options.join(''));
        this.$listSelector.val('');
    };

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.HiveListHandler = HiveListHandler;
    window.App = App;
})(window);