// Module for aggregating the list of selectors for the data entry form.
//
// Requirements:
//  - window: The global Window object; holds a reference
//              to the top-level namespace object.
//
(function(window) {
    'use strict';

    // fetch beehive-diary's top-level namespace object
    // and create it if it doesn't exist
    //
    var App = window.App || {};

    // this module's constructor - use with the 'new' keyword to
    // - create a new object
    // - refer to the new object within the function as 'this'
    // - automatically return the object as the value of the function
    //
    function UIElementSelectors() {
        this.HiveEntriesPanelSelector = '[data-beehive-diary="hive-entry-panel"]';
        this.HiveEntriesSelector = '[data-beehive-diary="hive-entry-list"]';
        this.HiveListSelector = '[data-beehive-diary="hive-list"]';
        this.FormSelector = '[data-beehive-diary="form"]';
    }

    // store the constructor in the namespace object
    // and make it available for use
    //
    App.UIElementSelectors = UIElementSelectors;
    window.App = App;
})(window);